" Add current mail to lbdb's database

augroup lbdb_mail
    au!

    au BufWritePost mutt-\w\+-\d\+-\d\+-\d\+
	\ if( getline(1) =~? '^From:' ) |
	\   call system( 'lbdb-fetchaddr < '..expand('%') ) |
	\ endif
augroup end

